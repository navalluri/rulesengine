import { Component, OnInit, Inject } from '@angular/core';
import { VoucherService } from '../../service/voucher.service';
import { MatSnackBar } from '@angular/material';
import * as $ from 'jquery';
import { UploadComponent } from '../upload/upload.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-vouchers',
  templateUrl: './vouchers.component.html',
  styleUrls: ['./vouchers.component.css']
})
export class VouchersComponent implements OnInit {
  value: string;
  selectedvouchers: string;
  selectLanguage;
  dynamicFilter;
  selectedCriteria: string;
  selectedFilterType: string;
  conditionsFilter;
  selecteCondition;
  selectedFilters = [];
  selectOptionFilter;
  vouchersData;
  selectedfilter;
  date;
  values;
  voucherToBeUpdate = [];
  filtersConditionArray = [];

  //voucherChecked;

  filterType = ["Match All Filters", "Match Any Filters"];
  inAndNotInFilter = ['in', 'not in'];
  moreAndLessFilter = ['more than', 'exactly', 'less than', 'has any value', 'in unknown'];
  criteria = ['Campaigns', 'Categories', 'Voucher type', 'Redeem Quantity', 'Start Date', 'Expired', 'Created', 'Updated', 'Campaign(Filter by Status)', 'Voucher(Filter by Status)'];
  compaignStatusFilter = ['is active', 'is before start date', 'expired', 'creation failed because of code generation', 'code generation is in progess'];
  voucherFiletr = ['is on', 'is off', 'expired', 'is active'];
  beforeAfterFilter = ['before', 'after', 'has any value', 'is unknown'];
  categoryFilter = [];
  CampaignsFilter = [];
  voucherTypeFilter = ['Discount', 'Gift'];
  isPopupOpened = true;
  notSelectedCriteria = true;
  notSelectedCondition = true;
  addFilterDisabled = true;
  removable = true;
  showSelectedFilter = false;
  showNoActiveFilterMessage = false;
  isDesc = true;
  orderBySelectedField = 'createdDate';

   constructor(public voucherService: VoucherService, public snackBar: MatSnackBar,public dialog: MatDialog) {
    this.getVouchers();
  }

  ngOnInit() {
    //this.getVouchers();
  }

  getVouchers() {
    this.voucherService
      .getVouchers()
      .subscribe(data => {
        this.vouchersData = data;
        if (this.vouchersData) {
          for (let i = 0; i < this.vouchersData.length; i++) {
            this.vouchersData[i].voucherChecked = false;
          }
        }

        //console.log("within init methos",this.vouchersData)
        this.categoryFilter = this.vouchersData.map(item => item.category);
        this.categoryFilter.push("Predefined Gift Cards", "Referral Reward");
        this.CampaignsFilter = this.vouchersData.map(item => item.compaign);
      });
  }

  onSelectCriteria(criteria) {
    this.selectedCriteria = criteria;
    this.notSelectedCriteria = false;

    if (criteria === 'Campaigns' || criteria === 'Categories' || criteria === 'Voucher type') {
      this.conditionsFilter = this.inAndNotInFilter;
    } else if (criteria === 'Redeem Quantity' || criteria === 'Start Date' || criteria === 'Expired') {
      this.conditionsFilter = this.moreAndLessFilter;
    } else if (criteria === 'Created' || criteria === 'Updated') {
      this.conditionsFilter = this.beforeAfterFilter;
    } else if (criteria === 'Campaign(Filter by Status)' || criteria === 'code generation is in progess') {
      this.conditionsFilter = this.compaignStatusFilter;
    }
  }

  onSelectConditions(condition) {
    this.selecteCondition = condition;
    this.notSelectedCondition = false;

    if (this.selectedCriteria === 'Campaigns') {
      this.dynamicFilter = this.CampaignsFilter;
    } else if (this.selectedCriteria === 'Categories') {
      this.dynamicFilter = this.categoryFilter;
    } else if (this.selectedCriteria === 'Voucher type') {
      this.dynamicFilter = this.voucherTypeFilter;
    }

    if (this.selectedCriteria === 'Campaign(Filter by Status)' || this.selectedCriteria === 'code generation is in progess') {
      this.addFilterDisabled = false;
    }

  }

  onSelectOptions(filter) {
    this.selectOptionFilter = filter;
    if (this.selectOptionFilter) {
      this.addFilterDisabled = false;
    }

  }

  onEnterValue(values) {
    this.addFilterDisabled = false;
  }

  onDateSelect(date) {
    this.addFilterDisabled = false;
  }

  onClickFilter() {
    this.showNoActiveFilterMessage = false;
    let criteria = this.selectedCriteria;
    let condition = this.selecteCondition;
    let options = this.selectOptionFilter ? this.selectOptionFilter : "";
    let chipFilterText = criteria + " " + condition + " " + options;
    this.selectedFilters.push(chipFilterText);

    //making conditions
    let filterConditionObject = {};
    filterConditionObject['criteria'] = criteria;
    if (criteria === 'Campaigns') {
      filterConditionObject['campaign'] = options ? options : null;
    }


    if (condition === 'in') {
      filterConditionObject['condition'] = '==';
    } else if (condition === 'not in') {
      filterConditionObject['condition'] = '!=';
    } else if (condition === 'not in') {
      filterConditionObject['condition'] = '!=';
    } else if (condition === 'more than') {
      filterConditionObject['condition'] = '>';
    } else if (condition === 'exactly') {
      filterConditionObject['condition'] = '==';
    } else if (condition === 'less than') {
      filterConditionObject['condition'] = '<';
    }

    this.filtersConditionArray.push(filterConditionObject);

    this.showSelectedFilter = true;

    this.selecteCondition = "";
    this.selectedCriteria = "";
    this.selecteCondition = "";
    this.selectedfilter = "";
    this.notSelectedCriteria = true;
    this.addFilterDisabled = true;
  }

  removefiltet(index) {
    this.selectedFilters.splice(index, 1);
    if (this.selectedFilters.length === 0) {
      this.showNoActiveFilterMessage = true;
    } else {
      this.showNoActiveFilterMessage = false;
    }
  }

  orderByField(fieldName) {
    this.orderBySelectedField = fieldName;
    this.isDesc = !this.isDesc;
    if (this.isDesc === true) {
      this.vouchersData = this.sortDescendingByKey(this.vouchersData, fieldName);
    } else {
      this.vouchersData = this.sortAscendingByKey(this.vouchersData, fieldName);
    }
  }

  sortAscendingByKey(array, key) {
    return array.sort(function(a, b) {
      var x = a[key];
      var y = b[key];
      return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
  }

  sortDescendingByKey(array, key) {
    return array.sort(function(a, b) {
      var x = a[key];
      var y = b[key];
      return ((x > y) ? -1 : ((x < y) ? 1 : 0));
    });
  }

  toggleVoucherSelection(voucherDetails) {
    if (voucherDetails.voucherChecked === undefined || voucherDetails.voucherChecked === false) {
      this.voucherToBeUpdate.push(voucherDetails);
      this.checkedSelectedVoucher(voucherDetails, true);
    } else {
      if (this.voucherToBeUpdate.length > 0) {
        for (var i = 0; i < this.voucherToBeUpdate.length; i++) {
          if (this.voucherToBeUpdate[i]._id === voucherDetails._id) {
            this.voucherToBeUpdate.splice(this.voucherToBeUpdate[i], 1);
          }
        }
      }
      this.checkedSelectedVoucher(voucherDetails, false);
    }
  }

  checkedSelectedVoucher(voucherDetails, checked) {
    for (let j = 0; j < this.vouchersData.length; j++) {
      if (this.vouchersData[j]._id === voucherDetails._id) {
        this.vouchersData[j].voucherChecked = checked;
      }
    }
  }

  enableSelectedVoucher() {
    if (this.voucherToBeUpdate.length > 0) {
      for (var i = 0; i < this.voucherToBeUpdate.length; i++) {
        this.voucherToBeUpdate[i].active = true;
      }
      this.voucherService.updateManyVouchers(this.voucherToBeUpdate).subscribe(res => {
        console.log("enabled updated successfully");
        this.openSnackBar("Enabled selected voucher successfully");
        this.voucherToBeUpdate = [];
        this.getVouchers();
      });
    }
  }

  disabledSelectedVoucher() {
    if (this.voucherToBeUpdate.length > 0) {
      for (var i = 0; i < this.voucherToBeUpdate.length; i++) {
        this.voucherToBeUpdate[i].active = false;
      }
      this.voucherService.updateManyVouchers(this.voucherToBeUpdate).subscribe(res => {
        console.log("disabled updated successfully");
        this.openSnackBar("Disabled selected voucher successfully");
        this.voucherToBeUpdate = [];
        this.getVouchers();
      })

    }
  }

  enabledOrDisableSingleVoucher(voucher) {
    let data = voucher;
    let dataArray = [];
    data.active = !voucher.active;
    dataArray.push(data);
    this.voucherService.updateManyVouchers(dataArray).subscribe(res => {
      console.log("enabled updated successfully")
      this.getVouchers();
    });
  }

  publishVoucher() {

  }

  displayVoucherQRCode() {

  }

  // to do : need to confirm before delete after delete show toast with proper message
  deleteVoucher(voucher) {
    this.voucherService.deleteVouchers(voucher._id).subscribe(res => {
      console.log('Deleted');
      this.openSnackBar("Voucher deleted successfully");
      this.getVouchers();
    });
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, 'Undo', {
      duration: 3000
    });
  }

  onSelectFilterType(filterType) {
    console.log("filter type is", filterType);
    if (this.filtersConditionArray.length === 0) {
      this.openSnackBar("Please add filters first");
    }
  }



    openDialog(): void {
      const dialogRef = this.dialog.open(UploadComponent, {
        width: '1000px',
    height: '412px',
    position: {top: '335px', left: '335px'}

       
      });
    }
  }
  /*unCheckedSelectedClient(client) {
    let clients = this.clients;
    for (let j = 0; j < clients.length; j++) {
      if (clients[j].clientId == client.clientId && client.clientChecked) {
        clients[j].clientChecked = false;
      } else if (clients[j].clientId == client.clientId) {
        clients[j].clientChecked = true;
      }
    }
  }*/


