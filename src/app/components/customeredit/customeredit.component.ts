import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {CustomersService} from '../../service/customers.service';
import {Customer} from '../../service/customer.model';
import { MatSnackBar } from '@angular/material';






@Component({
  selector: 'app-customeredit',
  templateUrl: './customeredit.component.html',
  styleUrls: ['./customeredit.component.css']
})
export class CustomereditComponent implements OnInit {

  id: String;
  customers: any = {};
  updateForm: FormGroup;

  // tslint:disable-next-line:max-line-length
  constructor(private customersService: CustomersService, private router: Router, private route: ActivatedRoute, private snackBar: MatSnackBar, private fb: FormBuilder) {
    this.createForm();
  }

  createForm() {
   
  
    this.updateForm=this.fb.group({
      name:['',Validators.required],
      phone:'',
      email:''
    })
  }

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.id = params.id;
      this.customersService.getCustomersById(this.id).subscribe(res => {
        this.customers = res;
        this.updateForm.get('name').setValue(this.customers.name);
        this.updateForm.get('phone').setValue(this.customers.phone);
        this.updateForm.get('email').setValue(this.customers.email);
      });
    });

  }


  UpdateCustomers(name, phone, email) {
    this.customersService.UpdateCustomers(this.id, name, phone, email).subscribe(() => {
      this.snackBar.open('Issue updated successfully', 'OK', {
        duration: 3000
      });
    });
  }

}
