import {  FileUploader, FileSelectDirective } from 'ng2-file-upload/ng2-file-upload';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { HttpClient, HttpResponse, HttpRequest, HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { of } from 'rxjs/observable/of';
import { catchError, last, map, tap } from 'rxjs/operators';
import {ErrorStateMatcher} from '@angular/material/core';

const URL = 'http://localhost:4000/api/upload';



  
@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css'],
  
 
})
export class UploadComponent implements OnInit {
  
  percentDone: number;
  uploadSuccess: boolean;
 
  
  title = 'app';
  public uploader: FileUploader = new FileUploader({url: URL, itemAlias: 'photo'});

  constructor( private http: HttpClient,) { }

  ngOnInit() {

    
     };




     upload(files: File[]){
      //pick from one of the 4 styles of file uploads below
      this.uploadAndProgress(files);
    }
  
    basicUpload(files: File[]){
      var formData = new FormData();
      Array.from(files).forEach(f => formData.append('file', f))
      this.http.post('', formData)
        .subscribe(event => {  
          console.log('done')
        })
    }
    
    //this will fail since file.io dosen't accept this type of upload
    //but it is still possible to upload a file with this style
    basicUploadSingle(file: File){    
      this.http.post('', file)
        .subscribe(event => {  
          console.log('done')
        })
    }
    
    uploadAndProgress(files: File[]){
      console.log(files)
      var formData = new FormData();
      Array.from(files).forEach(f => formData.append('file',f))
      
      this.http.post('', formData, {reportProgress: true, observe: 'events'})
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress) {
            this.percentDone = Math.round(100 * event.loaded / event.total);
          } else if (event instanceof HttpResponse) {
            this.uploadSuccess = true;
          }
      });
    }
    
    //this will fail since file.io dosen't accept this type of upload
    //but it is still possible to upload a file with this style
    uploadAndProgressSingle(file: File){    
      this.http.post('', file, {reportProgress: true, observe: 'events'})
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress) {
            this.percentDone = Math.round(100 * event.loaded / event.total);
          } else if (event instanceof HttpResponse) {
            this.uploadSuccess = true;
          }
      });
    }
  }