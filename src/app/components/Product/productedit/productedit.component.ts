import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { MatSnackBar } from '@angular/material';

import {ProductsService} from '../../../service/product.service';
import {product} from '../../../service/product.model';



@Component({
  selector: 'app-productedit',
  templateUrl: './productedit.component.html',
  styleUrls: ['./productedit.component.css']
})
export class ProducteditComponent implements OnInit {

id: String;
  products : any = {};
  updateForm1: FormGroup;


  constructor(private productsService: ProductsService, private router: Router, private route: ActivatedRoute, private snackBar: MatSnackBar, private fb: FormBuilder) { 

     this.createForm();
  }

  createForm() {
   
  
    this.updateForm1=this.fb.group({
      pname:['',],
      price:'',
      pid:''
    })
  }

  ngOnInit() {

    this.route.params.subscribe(params => {
        this.id = params.id;
        this.productsService.getProductsById(this.id).subscribe(res1 => {
        this.products = res1;
        this.updateForm1.get('pname').setValue(this.products.pname);
        this.updateForm1.get('price').setValue(this.products.price);
        this.updateForm1.get('pid').setValue(this.products.pid);
      });
    });

  }


  UpdateProducts(pname, price, pid) {
    this.productsService.UpdateProducts(this.id, pname, price, pid).subscribe(() => {
      this.snackBar.open('product updated successfully', 'OK', {
        duration: 3000
      });
    });
  }

}
