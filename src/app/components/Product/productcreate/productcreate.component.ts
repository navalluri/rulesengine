import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router'; 
import { MatPaginator, MatSort } from '@angular/material';
import { MatTableDataSource} from '@angular/material';
//import { IssueService } from '../../issue.service';

//import {CustomersService} from '../../service/customers.service';

import {ProductsService} from '../../../service/product.service';



@Component({
  selector: 'app-productcreate',
  templateUrl: './productcreate.component.html',
  styleUrls: ['./productcreate.component.css']
})

export class ProductcreateComponent implements OnInit {


  createForm: FormGroup;

  constructor(private productsService : ProductsService, private fb:FormBuilder,private router:Router ) { 

    this.createForm=this.fb.group({
      pname:['',Validators.required],
      price:'',
      pid:''
    })
    
  }

addProducts(pname,price,pid){

this.productsService.addProducts(pname,price,pid).subscribe(()=>{

  this.router.navigate(['/Distributions']);
});

 

}

 ngOnInit() {
}

}