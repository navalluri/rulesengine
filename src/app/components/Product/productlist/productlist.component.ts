import { Component, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/table';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material';
import {ProductsService} from '../../../service/product.service';

import {product} from '../../../service/product.model';


@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.css']
})
export class ProductlistComponent implements OnInit {

  products : product[];

  displayedColumns = ['Name', 'price', 'pid',  'actions'];

  constructor(private ProductsService: ProductsService, private router: Router) { }

  ngOnInit() {
    this.fetchProducts();
  }


  fetchProducts() {
    this.ProductsService
      .getProducts()
      .subscribe((data: product[]) => {
        this.products = data;
        console.log('Data requested ...');
        console.log(this.products);
      });
  }

  editProduct(id) {
    this.router.navigate([`/edit/${id}`]);
  }

  deleteProduct(id) {
    this.ProductsService.deleteProduct(id).subscribe(() => {
      this.fetchProducts();
    });
  }

}
