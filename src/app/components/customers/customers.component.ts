import { Component, OnInit } from '@angular/core';
import {SelectionModel} from '@angular/cdk/collections';
import {MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})

export class CustomersComponent implements OnInit {
  value: string;
  selectedvouchers: string;
selectLanguage;
Customersdata =["All Customers","Customers joined in September 2018","Customers who Abondoned the cart","New customers"];

CustomersFilters = ["Match any filters","Match all Filters"];
isPopupOpened = true;

  constructor() { }

  ngOnInit() {
  }

}
