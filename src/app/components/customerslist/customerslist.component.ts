import { Component, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/table';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material';
import {CustomersService} from '../../service/customers.service';
import {Customer} from '../../service/customer.model';
//import { Issue } from '../../issue.model';
//import { IssueService } from '../../issue.service';

@Component({
  selector: 'app-customerslist',
  templateUrl: './customerslist.component.html',
  styleUrls: ['./customerslist.component.css']
})



export class CustomerslistComponent implements OnInit {
  customers : Customer[];
  //issues: Issue[];
  displayedColumns = ['name', 'phone', 'email',  'actions'];

  constructor(private customersService: CustomersService, private router: Router) { }

  ngOnInit() {

    this.fetchCustomers();
   // this.customersService.getCustomers().subscribe((customers)=>{
      //  console.log(customers);

    }
  

  fetchCustomers() {
    this.customersService
      .getCustomers()
      .subscribe((data: Customer[]) => {
        this.customers = data;
        console.log('Data requested ...');
        console.log(this.customers);
      });
  }

  editIssue(id) {
    this.router.navigate([`/edit/${id}`]);
  }

  deleteIssue(id) {
    this.customersService.deleteCustomer(id).subscribe(() => {
      this.fetchCustomers();
    });
  }
}

