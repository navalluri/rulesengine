import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatevouchersComponent } from './createvouchers.component';

describe('CreatevouchersComponent', () => {
  let component: CreatevouchersComponent;
  let fixture: ComponentFixture<CreatevouchersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatevouchersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatevouchersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
