import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material';
import { FormControl } from '@angular/forms';
import * as $ from 'jquery';
import { VoucherService } from '../../service/voucher.service';

@Component({
  selector: 'app-createvouchers',
  templateUrl: './createvouchers.component.html',
  styleUrls: ['./createvouchers.component.css']

})


export class CreatevouchersComponent {
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  Checked: boolean;
  Indeterminate: boolean;
  LabelAlign: string;
  IsDisabled: boolean;

  //form model 
  step = 0;
  productSelectedIndex;
  voucherTypeSelectedIndex;
  openRelatedOptions;
  voucherType: string;
  productDetails: string = 'Bulk codes';
  createVoucherDataObject = {};
  discountVoucherValue: number;
  discountType: string;
  giftVoucherValue: number;
  compaign: string;
  voucherCount: number;
  prefix: string;
  category: string;
  voucherTypeTitle = "Choose voucher Types";
  // giving the default value 


  myControl = new FormControl();
  options: string[] = ['Apple Iphone', 'Oneplus6', 'Iphonex'];
  value: string;
  selectedvouchers: string;
  selectLanguage;
  Customersdata = ["All Customers", "Customers joined in September 2018", "Customers who Abondoned the cart", "New customers"];

  CustomersFilters = ["Match any filters", "Match all Filters"];
  myControl1 = new FormControl();
  options1: string[] = ['One', 'Two', 'Three'];

  isPopupOpened = true;

  constructor(private _formBuilder: FormBuilder, public voucherService: VoucherService) {}

  ngOnInit() {

    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });

    $(document).ready(function() {
      $("section.hideme").hide();
      $("button.b").click(function() {
        var id = $(this).attr("id");
        var sectionId = id.replace("b", "section");
        $("section.hideme").hide();
        $("section#" + sectionId).fadeIn("slow");
      });
    });
  }

  createVoucher() {
    this.createVoucherDataObject['discountType'] = this.discountType ? this.discountType : "";
    this.createVoucherDataObject['voucherType'] = this.voucherType ? this.voucherType : "";
    this.createVoucherDataObject['discountVoucherValue'] = this.discountVoucherValue ? this.discountVoucherValue : "";
    this.createVoucherDataObject['giftVoucherValue'] = this.giftVoucherValue ? this.giftVoucherValue : "";
    this.createVoucherDataObject['compaign'] = this.compaign ? this.compaign : "";
    this.createVoucherDataObject['voucherCount'] = this.voucherCount ? this.voucherCount : "";
    this.createVoucherDataObject['prefix'] = this.prefix ? this.prefix : "";
    this.createVoucherDataObject['category'] = this.category ? this.category : "";
    this.createVoucherDataObject['productDetails'] = this.productDetails;
    this.createVoucherDataObject['createdBy'] = "demo";
    this.voucherService.createVouchers(this.createVoucherDataObject);
  }

  setStep(index: number) {
    this.step = index;
  }

  onChooseVoucherType(voucherType, index) {
    this.voucherType = voucherType;
    this.voucherTypeSelectedIndex = index;
  }

  onSelectType(index, productDetails) {
    this.openRelatedOptions = index;
    this.productDetails = productDetails;
    this.productSelectedIndex = index;
    if (productDetails === "Referral program") {
      this.voucherTypeTitle = "Define the incentive for a new customer";
    } else if (productDetails === "Bulk codes") {
      this.voucherTypeTitle = "Choose voucher Types";
    }
  }

  form1() {
    console.log(this.firstFormGroup.value);
  }

  form2() {
    console.log(this.secondFormGroup.value);
  }


}
