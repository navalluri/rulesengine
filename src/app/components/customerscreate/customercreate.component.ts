import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router'; 
import { MatPaginator, MatSort } from '@angular/material';
import { MatTableDataSource} from '@angular/material';
//import { IssueService } from '../../issue.service';

import {CustomersService} from '../../service/customers.service';

@Component({
  selector: 'app-customercreate',
  templateUrl: './customercreate.component.html',
  styleUrls: ['./customercreate.component.css']
})


export class CustomercreateComponent implements OnInit {

  createForm: FormGroup;

  constructor(private customersService: CustomersService, private fb:FormBuilder,private router:Router ) { 

    this.createForm=this.fb.group({
      name:['',Validators.required],
      phone:'',
      email:''
    })
    
  }

addCustomers(name,phone,email){

this.customersService.addCustomers(name,phone,email).subscribe(()=>{

  this.router.navigate(['/campaigns']);
});

}

  ngOnInit() {
  }

}
