import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VoucherService {

  uri = 'http://localhost:4000/vouchers';

  constructor(private http: HttpClient) {}

  createVouchers(data) {
    this.http.post(`${this.uri}/add`, data)
      .subscribe(res => console.log('Voucher save successfully'));
  }

  getVouchers() {
    return this
      .http
      .get(`${this.uri}`);
  }

  editVouchers(id) {
    return this
      .http
      .get(`${this.uri}/edit/${id}`);
  }

  updateVouchers(data, id) {
    this
      .http
      .post(`${this.uri}/update/${id}`, data)
      .subscribe(res => console.log('Done'));
  }

  updateManyVouchers(data) {
    return this
      .http
      .post(`${this.uri}/update/`, data);
  }

  deleteVouchers(id) {
    return this
      .http
      .get(`${this.uri}/delete/${id}`);
  }
}
