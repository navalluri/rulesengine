import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  uri ='http://localhost:4000/products';

  constructor(private http:HttpClient) { }

  getProducts(){

    return this
           .http
           .get(`${this.uri}`);
  }


  getProductsById(id){
    
    return this.http.get(`${this.uri}/edit/${id}`);

  }

  addProducts(pname,price,pid){
    const product ={
      pname:pname,
      price:price,
      pid : pid
    };
    return this.http.post(`${this.uri}/add`,product);
  }


 UpdateProducts(id,pname,price,pid){
    const product ={
      pname:pname,
      price:price,
      pid : pid
    };
    return this.http.post(`${this.uri}/update/${id}`,product);
  }


  deleteProduct(id){
    return this.http.get(`${this.uri}/delete/${id}`);
  }
}
