import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  uri ='http://localhost:4000/newcustomers';

  constructor(private http:HttpClient) { }

  getCustomers(){

    return this
           .http
           .get(`${this.uri}`);
  }


  getCustomersById(id){
    return this.http.get(`${this.uri}/edit/${id}`);

  }

  addCustomers(name,email,phone){
    const customer ={
      name:name,
      email:email,
      phone : phone
    };
    return this.http.post(`${this.uri}/add`,customer);
  }


 UpdateCustomers(id,name,email,phone){
    const customer ={
      name:name,
      email:email,
      phone : phone
    };
    return this.http.post(`${this.uri}/update/${id}`,customer);
  }


  deleteCustomer(id){
    return this.http.get(`${this.uri}/delete/${id}`);
  }
}
