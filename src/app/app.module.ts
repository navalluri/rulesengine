import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatGridListModule, MatCardModule, MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import {MatSelectModule,MatOptionModule,MatCheckboxModule} from '@angular/material';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { MatFileUploadModule } from 'angular-material-fileupload';
import { FileSelectDirective, FileDropDirective } from 'ng2-file-upload';
import {MatStepperModule} from '@angular/material/stepper';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';
import {MatTabsModule} from '@angular/material/tabs';
import {MatChipsModule} from '@angular/material/chips';
import {MatRadioModule} from '@angular/material/radio';
import {MatDividerModule,MatSnackBarModule} from '@angular/material';




// Add the components 
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CreateComponent } from './components/create/create.component';
import { IndexComponent } from './components/index/index.component';
import { EditComponent } from './components/edit/edit.component';
import { MyDashboardComponent } from './components/my-dashboard/my-dashboard.component';
import { MyTableComponent } from './components/my-table/my-table.component';
import { VouchersComponent } from './components/vouchers/vouchers.component';
import { ProductsComponent } from './components/products/products.component';
import { OrdersComponent } from './components/orders/orders.component';
import { UploadComponent } from './components/upload/upload.component';
import { CreatevouchersComponent } from './components/createvouchers/createvouchers.component';
import { CustomersComponent } from './components/customers/customers.component';
import { CustomerstableComponent } from './components/customerstable/customerstable.component';
import { CreatecustomersComponent } from './components/createcustomers/createcustomers.component';
import { RedemptionsComponent } from './components/redemptions/redemptions.component';
import { RedeemComponent } from './components/redeem/redeem.component';
import { ProductdetailsComponent } from './components/productdetails/productdetails.component';
import { CustomerdetailsComponent } from './components/customerdetails/customerdetails.component';
import { CreateproductsComponent } from './components/createproducts/createproducts.component';



import { CdkTableModule} from '@angular/cdk/table';
//import {DataSource} from '@angular/cdk/table';
import { CommonModule } from "@angular/common";
import 'hammerjs';
import {  MatProgressSpinnerModule } from '@angular/material';


import {
     MatTabHeader,
  MatHeaderRow, MatHeaderCell, MatHeaderCellDef, MatHeaderRowDef,
 MatRow, MatRowDef,  MatCell, MatCellDef,
  
} from '@angular/material';
//backend service
import { AdunitService } from './service/adunit.service';
import { CustomerslistComponent } from './components/customerslist/customerslist.component';
import { CustomercreateComponent } from './components/customerscreate/customercreate.component';
import {CustomersService} from './service/customers.service';
import { CustomereditComponent } from './components/customeredit/customeredit.component';
import { ProductcreateComponent } from './components/Product/productcreate/productcreate.component';
import { ProductlistComponent } from './components/Product/productlist/productlist.component';
import { ProducteditComponent } from './components/Product/productedit/productedit.component';

const routes: Routes = [
  {
    path: 'create',
    component: CustomercreateComponent
  },
  {
    path: 'edit/:id',
    component: CustomereditComponent
  },
  {
    path: 'index',
    component: IndexComponent
  },
  {
    path: 'dashboard',
    component: MyDashboardComponent
  },
  {
    path: 'voucher',
    component: VouchersComponent
  },
  {
    path:'campaigns',
    component: CustomerslistComponent
  },
  {
    path:'Distributions',
    component: ProductlistComponent
  },

  {
    path:'redemptions',
    component: RedemptionsComponent
  },
  {
    path:'customers',
    component: CustomerslistComponent
  },
  {
    path:'products',
    component: ProductlistComponent
  },
  {
    path:'orders',
    component: OrdersComponent
  },
  {
    path:'createVouchers',
    component: CreatevouchersComponent
  },
{
    path:'createProducts',
    component: ProductcreateComponent
  },
  {
    path:'editProducts/:id',
    component: ProducteditComponent
  },

  {
    path:'RedeemVoucher',
    component: RedeemComponent
  },
];






@NgModule({
  declarations: [
    AppComponent,
    CreateComponent,
    IndexComponent,
    EditComponent,
    FileSelectDirective,
    NavbarComponent,
    MyDashboardComponent,
    MyTableComponent,
    VouchersComponent,
    ProductsComponent,
    OrdersComponent,
    UploadComponent,
    CreatevouchersComponent,
    CreateproductsComponent,
    CustomersComponent,
    CustomerstableComponent,
    CreatecustomersComponent,
    RedemptionsComponent,
    RedeemComponent,
    ProductdetailsComponent,
    CustomerdetailsComponent,
    CustomerslistComponent,
    CustomercreateComponent,
    CustomereditComponent,
    ProductcreateComponent,
    ProductlistComponent,
    ProducteditComponent,

  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    SlimLoadingBarModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatOptionModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatSelectModule,
    MatStepperModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    BrowserAnimationsModule,
    MatButtonModule,
     MatCheckboxModule,
     MatExpansionModule,
     MatFormFieldModule,
     MatInputModule,
     MatIconModule,
     MatToolbarModule,
     MatTabsModule,
     MatChipsModule,
     MatAutocompleteModule,
     MatRadioModule,
     MatSlideToggleModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    CdkTableModule,
      
 CdkTableModule,  MatTableModule,
 MatDividerModule,MatSnackBarModule
  
  ],
  providers: [ AdunitService, CustomersService, MatDatepickerModule ],
  bootstrap: [AppComponent],
   entryComponents:[UploadComponent]
})
export class AppModule { }
