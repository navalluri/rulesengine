const express = require('express'),
    path = require('path'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    mongoose = require('mongoose'),
    config = require('./config/DB');

    const app = express();

    mongoose.Promise = global.Promise;

    //database connections
    mongoose.connect(config.DB).then(
      () => {console.log('Database is connected') },
      err => { console.log('Can not connect to the database'+ err)}
    );

    // add all the routes 
    const adUnitRoutes = require('./routes/adunit.route');
    const createVoucherRoutes = require('./routes/voucher.route');
    const createProductRoutes = require('./routes/Product.route');
    const createCustomerRoutes = require('./routes/customer.route');
    const createnewcustomerRoutes = require('./routes/newcustomer.route');


    app.use(bodyParser.json());
    app.use(cors());
    const port = process.env.PORT || 4000;

    // defining the routes subpath
    app.use('/adunits', adUnitRoutes);
    app.use('/vouchers', createVoucherRoutes);
    app.use('/products',createProductRoutes);
    app.use('/Customers',createCustomerRoutes)
    app.use('/newcustomers',createnewcustomerRoutes)

    const server = app.listen(port, function(){
     console.log('Listening on port ' + port);
    });