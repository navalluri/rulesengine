// ProductModel.route.js

const express = require('express');
const app = express();
const ProductRoutes = express.Router();

// Require create  model in our routes module
let productModel = require('../models/Product.model');

// Save Products
ProductRoutes.route('/add').post(function (req, res) {
    let Product = new productModel(req.body);
    Product.save()
      .then(game => {
      res.status(200).json({'Product': 'Product is added successfully'});
      })
      .catch(err => {
      res.status(400).send("unable to save to database");
      });
  });

 // To Get all Product Details
 ProductRoutes.route('/').get(function (req, res) {
    productModel.find(function (err, Products){
    if(err){
      console.log(err);
    }
    else {
      res.json(Products);
    }
  });
});
//  edit route
ProductRoutes.route('/edit/:id').get(function(req, res) {
    let id = req.params.id;
    productModel.findById(id, function(err, Products) {
      res.json(Products);
    });
  });


  // Defined delete | remove | destroy route
  ProductRoutes.route('/delete/:id').get(function(req, res) {
    productModel.findByIdAndRemove({ _id: req.params.id }, function(err, Products) {
      if (err) res.json(err);
      else res.json('Successfully removed');
    });
  });

//  Defined update route
ProductRoutes.route('/update/:id').post(function (req, res) {
    productModel.findById(req.params.id, function(err, Products) {
    if (!Products)
      return next(new Error('Could not load Document'));
    else {
        Products.pname = req.body.pname;
      
     
        Products.price = req.body.price;
        Products.pid = req.body.pid;
       

        
        Products.save().then(Products => {
          res.json('Update complete');
      })
      .catch(err => {
            res.status(400).send("unable to update the database");
      });
    }
  });
});


  module.exports = ProductRoutes;