// ProductModel.route.js

const express = require('express');
const app = express();
const CustomerRoutes = express.Router();

// Require create  model in our routes module
let CustomerModel = require('../models/customer.model');


// Save Products
CustomerRoutes.route('/add').post(function (req, res) {
    let Customer = new CustomerModel(req.body);
    Customer.save()
      .then(game => {
      res.status(200).json({'Customer': 'Customer is added successfully'});
      })
      .catch(err => {
      res.status(400).send("unable to save to database");
      });
  });


  // To Get all Product Details
  CustomerRoutes.route('/').get(function (req, res) {
    CustomerModel.find(function (err, Customers){
    if(err){
      console.log(err);
    }
    else {
      res.json(Customers);
    }
  });
});


//  edit route
CustomerRoutes.route('/edit/:id').get(function(req, res) {
    let id = req.params.id;
    CustomerModel.findById(id, function(err, Customers) {
      res.json(Customers);
    });
  });



  // Defined delete | remove | destroy route
  CustomerRoutes.route('/delete/:id').get(function(req, res) {
    CustomerModel.findByIdAndRemove({ _id: req.params.id }, function(err, Customers) {
      if (err) res.json(err);
      else res.json('Successfully removed');
    });
  });

//  Defined update route
CustomerRoutes.route('/update/:id').post(function (req, res) {
    CustomerModel.findById(req.params.id, function(err, Customers) {
    if (!Customers)
      return next(new Error('Could not load Document'));
    else {
        Customers.customerNAME = req.body.customerNAME;
        Customers.customerId = req.body.customerId;
       
        Customers.customerDesc = req.body.customerDesc;
        Customers.email = req.body.email;
        Customers.phone = req.body.phone;
        Customers.Address1 = req.body.Address1;

        
        Customers.save().then(Customers => {
          res.json('Update complete');
      })
      .catch(err => {
            res.status(400).send("unable to update the database");
      });
    }
  });
});


  module.exports = CustomerRoutes;