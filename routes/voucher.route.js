// voucherModel.route.js

const express = require('express');
const app = express();
const voucherRoutes = express.Router();

// Require create  model in our routes module
let voucherModel = require('../models/voucher.model');

// Defined store route
voucherRoutes.route('/add').post(function(req, res) {
  let data = req.body;
  let voucherArray = [];

  let voucherCount = data.voucherCount ? data.voucherCount : 0;
  if (voucherCount > 0) {
    for (let i = 0; i < voucherCount; i++) {
      let data = {};
      data.voucherType = req.body.voucherType ? req.body.voucherType : "";
      data.discountType = req.body.discountType ? req.body.discountType : "";
      data.discountVoucherValue = req.body.discountVoucherValue ? req.body.discountVoucherValue : "";
      data.voucherType = req.body.voucherType ? req.body.voucherType : "";
      data.giftVoucherValue = req.body.giftVoucherValue ? req.body.giftVoucherValue : "";
      data.compaign = req.body.compaign ? req.body.compaign : "";
      data.voucherCount = req.body.voucherCount ? req.body.voucherCount : "";
      data.productDetails = req.body.productDetails ? req.body.productDetails : "";
      data.prefix = req.body.prefix ? req.body.prefix : "";
      data.category = req.body.category ? req.body.category : "";
      data.voucherCode = req.body.prefix ? req.body.prefix + uniqueID() : uniqueID();
      voucherArray.push(data);
    }
    voucherModel.insertMany(voucherArray)
      .then(game => {
        res.status(200).json({ 'voucher created': 'voucher created in added successfully' });
      })
      .catch(err => {
        res.status(400).send("unable to save vouchers to database");
      });
  } else {
    data.voucherCode = req.body.prefix ? req.body.prefix + uniqueID() : uniqueID();
    let voucher = new voucherModel(data);
    voucher.save()
      .then(game => {
        res.status(200).json({ 'voucher created': 'voucher created in added successfully' });
      })
      .catch(err => {
        res.status(400).send("unable to save vouchers to database");
      });
  }

});

// Defined get data(index or listing) route
voucherRoutes.route('/').get(function(req, res) {
  voucherModel.find(function(err, vouchers) {
    if (err) {
      console.log(err);
    } else {
      res.json(vouchers);
    }
  });
});

// Defined edit route
voucherRoutes.route('/edit/:id').get(function(req, res) {
  let id = req.params.id;
  voucherModel.findById(id, function(err, vouchers) {
    res.json(vouchers);
  });
});

//  Defined update route
voucherRoutes.route('/update/:id').post(function(req, res, next) {
  voucherModel.find({ "_id": req.body.id }, function(err, vouchers) {
    if (!vouchers)
      return next(new Error('Could not load data for updating '));
    else {
      voucherModel.updateMany({ "_id": req.params.id }, req.body).then(data => {
          res.json('Vouchers update completed');
        })
        .catch(err => {
          res.status(400).send("unable to update the database");
      });
    }
  });
});

// Defined delete | remove | destroy route
voucherRoutes.route('/delete/:id').get(function(req, res) {
  voucherModel.findByIdAndRemove({ _id: req.params.id }, function(err, vouchers) {
    if (err) res.json(err);
    else res.json('Successfully removed');
  });
});


//updating only active based on id of the voucher
voucherRoutes.route('/update/').post(function(req, res, next) {
  let enablingData = req.body;
  for(let i=0;i<enablingData.length;i++){
    voucherModel.update({ "_id": enablingData[i]._id },{ $set:{"active": enablingData[i].active}}).then(data => {
      console.log("active data updated successfully and id is",enablingData[i]._id );
      if(i === enablingData.length-1){
        res.json('Vouchers update completed');
      }    
    })
    .catch(err => {
      res.status(400).send("unable to update the database");
    });
  }
});

// need to arrange this function
function uniqueID() {
  function chr4() {
    return Math.random().toString(32).slice(-2);
  }
  return chr4() + chr4() +
    '-' + chr4() + chr4() +
    '-' + chr4() + chr4() +
    '-' + chr4() + chr4() +
    '-' + chr4() + chr4() +
    '-' + chr4() + chr4();
}


module.exports = voucherRoutes;
