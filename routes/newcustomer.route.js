// ProductModel.route.js

const express = require('express');
const app = express();
const newcustomerRoutes = express.Router();

// Require create  model in our routes module
let newcustomerModel = require('../models/newcustomer.model');

// Save Products
newcustomerRoutes.route('/add').post(function (req, res) {
    let Customer = new newcustomerModel(req.body);
    Customer.save()
      .then(game => {
      res.status(200).json({'Customer': 'Customer is added successfully'});
      })
      .catch(err => {
      res.status(400).send("unable to save to database");
      });
  });

 // To Get all Product Details
 newcustomerRoutes.route('/').get(function (req, res) {
    newcustomerModel.find(function (err, Customers){
    if(err){
      console.log(err);
    }
    else {
      res.json(Customers);
    }
  });
});
//  edit route
newcustomerRoutes.route('/edit/:id').get(function(req, res) {
    let id = req.params.id;
    newcustomerModel.findById(id, function(err, Customers) {
      res.json(Customers);
    });
  });


  // Defined delete | remove | destroy route
  newcustomerRoutes.route('/delete/:id').get(function(req, res) {
    newcustomerModel.findByIdAndRemove({ _id: req.params.id }, function(err, Customers) {
      if (err) res.json(err);
      else res.json('Successfully removed');
    });
  });

//  Defined update route
newcustomerRoutes.route('/update/:id').post(function (req, res) {
    newcustomerModel.findById(req.params.id, function(err, Customers) {
    if (!Customers)
      return next(new Error('Could not load Document'));
    else {
        Customers.name = req.body.name;
      
     
        Customers.email = req.body.email;
        Customers.phone = req.body.phone;
       

        
        Customers.save().then(Customers => {
          res.json('Update complete');
      })
      .catch(err => {
            res.status(400).send("unable to update the database");
      });
    }
  });
});


  module.exports = newcustomerRoutes;