const mongoose = require('mongoose');
const Schema = mongoose.Schema;


// Define collection and schema for Products
let CustomerSchema = new Schema({
    customerNAME: {
      type: String
    },
    customerId: {
      type: String
    },
    customerDesc: {
        type: String
      },
      email: {
        type: String
      },
      phone: {
        type: String
      },
      Address1: {
        type: String
      },
      Address2: {
        type: String
      },
      postalcode: {
        type: String
      },
      city: {
        type: String
      },
      state: {
        type: String
      },
      country: {
        type: String
      }
  
},{
      collection: 'customers'
  });
  


  module.exports = mongoose.model('customer', CustomerSchema);
