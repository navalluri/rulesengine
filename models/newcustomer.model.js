const mongoose = require('mongoose');
const Schema = mongoose.Schema;


// Define collection and schema for customers
let NewCustomerSchema = new Schema({
    name: {
      type: String
    },
    
      email: {
        type: String
      },
      phone: {
        type: String
      },
     
  },{
      collection: 'newcustomers'
  });
  



  module.exports = mongoose.model('newcustomer', NewCustomerSchema);
