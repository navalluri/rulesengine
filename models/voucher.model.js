const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema for create voucher
/*let voucherSchema = new Schema({
  productDetails:{type:String},
  voucherCode:{type:String},
  voucherType: { type: Object },
  details: { type: Object },
  productCode: { type: Object },
  limitForVoucher: { type: Object },
  compaignMetaData: { type: Object },
  promotionCompaignDetails: { type: Object },
  promotionTiering: { type: Object },
  rewardForReferrer: { type: Object }
}, {
  collection: 'vouchers' // colletion name means table name 
});*/

  
let voucherSchema = new Schema({
  productDetails:{type:String},
  voucherCode:{type:String},
  discountType: { type: String },
  voucherType: { type: String },
  discountVoucherValue: { type: String },
  giftVoucherValue: { type: String },
  compaign: { type: String },
  voucherCount: { type: String },
  prefix: { type: String },
  category: { type: String },
  active:{type:Boolean, default:true},
  createdBy:{type:String},
  createdDate:{type:String, default: new Date()}
}, {
  collection: 'vouchers' // colletion name means table name 
});



// exporting the schema so that it can be use in any where in the application
module.exports = mongoose.model('Voucher', voucherSchema);
